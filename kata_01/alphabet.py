class Alphabet(object):

    def __init__(self, dictionary_chars='abcdefghijklmnopqrstuvwxyz',
                 encrypted_chars='!)"(£*%&><@abcdefghijklmno',
                 escape_char='#'):
        self.dictionary_chars = dictionary_chars
        self.encrypted_chars = encrypted_chars
        self.escape_char = escape_char

    def encrypt_char(self, character):
        try:
            index = self.dictionary_chars.index(character)
            return self.encrypted_chars[index]
        except (ValueError):
            return self.escape_char + character

    def encrypt(self, message):
        encrypted = ""
        for character in message:
            encrypted += self.encrypt_char(character)

        return encrypted

    def decrypt(self, message):
        decrypted = ""
        flag = False
        for character in message:
            if character == self.escape_char:
                flag = True
            elif flag:
                decrypted += character
                flag = False
            else:
                try:
                    index = self.encrypted_chars.index(character)
                    decrypted += self.dictionary_chars[index]
                except (ValueError):
                    decrypted += character
        return decrypted
