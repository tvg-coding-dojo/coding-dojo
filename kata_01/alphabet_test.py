import unittest

from alphabet import Alphabet


class AlphabetTest(unittest.TestCase):
    # def __init__(self):

    def setUp(self):
        self.alphabet = Alphabet()

    def test_diff(self):
        assert self.alphabet.encrypt("message") is not "message"

    def test_message_encrypt(self):
        self.assertEqual(self.alphabet.encrypt("message"), "b£hh!%£")

    def test_teste_encrypt(self):
        self.assertEqual(self.alphabet.encrypt("test"), "i£hi")

    def test_encrypt_with_unknown_char(self):
        self.assertEqual(self.alphabet.encrypt("test1"), "i£hi#1")

    def test_encrypt_with_encrypted_unique_char(self):
        self.assertEqual(self.alphabet.encrypt("test!"), "i£hi#!")

    def test_ecrypt_with_key_character(self):
        self.assertEqual(self.alphabet.encrypt("test£"), "i£hi#£")

    def test_message_decrypt(self):
        self.assertEqual(self.alphabet.decrypt("b£hh!%£"), "message")

    def test_decrypt_another_message(self):
        self.assertEqual(self.alphabet.decrypt("i£hi"), "test")

    def test_decrypt_message_with_key_symbol(self):
        self.assertEqual(self.alphabet.decrypt("i£hi1"), "test1")

    def test_decrypt_encrypted_message(self):
        message = 'teste'
        encrypted = self.alphabet.encrypt(message)
        decrypted = self.alphabet.decrypt(encrypted)

        self.assertEqual(message, decrypted)

    def test_decrypt_encrypted_message_with_encrypetd_char(self):
        message = 'teste£'
        encrypted = self.alphabet.encrypt(message)
        decrypted = self.alphabet.decrypt(encrypted)

        self.assertEqual(message, decrypted)

    def test_encrypt_decrypt_message(self):
        message = 'i£hi'
        decrypted = self.alphabet.decrypt(message)
        encrypted = self.alphabet.encrypt(decrypted)

        self.assertEqual(message, encrypted)

if __name__ == '__main__':
    unittest.main()
